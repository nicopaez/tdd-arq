require 'sinatra'
require_relative './model/recomendador'
require_relative './model/adapter_clima'

before do
  content_type 'application/json'
end

get '/temperature' do
  adapter_clima = AdapterClima.new
  recomender = Recomendador.new(adapter_clima)
  recomendacion = recomender.recomendar()
  status 200
  { :temperature => recomendacion}.to_json
end
