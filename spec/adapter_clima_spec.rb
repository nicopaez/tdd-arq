require 'spec_helper'
require_relative '../model/adapter_clima'

describe 'AdapterClima' do

  it 'deberia invocar a la api y obtener una temp para baires entre -20 y 50' do
    adapter = AdapterClima.new
    lat = -34
    lon = -58
    temp = adapter.get_temperatura(lat, lon)
    expect(temp).to be_between(-20, 50)
  end

end

# ruby => app.rb => recomendador(temp, pref) => climaAdapter => api
