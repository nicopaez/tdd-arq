require 'spec_helper'
require_relative '../model/recomendador'
require_relative './adaptador_mentira'

describe 'Recomendador' do

  it 'deberia invocar al adapter' do
    temp = 8
    adapterClima = AdaptadorMentira.new(temp)
    recomendador = Recomendador.new(adapterClima)
    recomendacion = recomendador.recomendar()
    expect(recomendacion).to eq temp
  end

  it 'deberia redondear la temperatura al adapter' do
    temp = 8.777777777
    adapterClima = AdaptadorMentira.new(temp)
    recomendador = Recomendador.new(adapterClima)
    recomendacion = recomendador.recomendar()
    expect(recomendacion).to eq 9
  end



end
