Given("I am in Buenos Aires") do
  # nothing to do here, Buenos Aires is the default location of the app
end

When("I ask the temperature") do
  get '/temperature'
end

Then("I should get a value between {int}  and {int}") do |min, max|
  data = JSON.parse last_response.body
  temperature = data['temperature']
  expect(temperature).to be_between(min, max)
end
