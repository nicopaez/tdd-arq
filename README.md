Recomendador de ropa
====================

En base a la temperatura actual de Buenos Aires y las preferencias de vestido la aplicación debe recomendar una prenda.

* Preferencias: liviana | pesada
* pref:liviana & temp > 20 bermuda
* pref:liviana & temp <= 20 remera
* pref:liviana & temp <= 10 buzo
* pref:pesada & temp > 20 jean
* pref:pesada & temp <= 20 camisa
* pref:pesada & temp <= 10 campera


## Preparación del ambiente

sudo apt-get update
sudo apt-get install build-essential
sudo apt-get install -y git
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm
rvm install 2.5.7
gem install bundler

## Uso del proyecto

Una vez realizada la instalación:

1. Instalar las dependencias del proyecto ejecutando _bundle install_
2. Ejecutar las pruebas ejecutando _bundle exec rake_
