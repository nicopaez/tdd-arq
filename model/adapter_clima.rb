require 'httpclient'
require 'json'

class AdapterClima

  def get_temperatura(lat, lon)
    api_key = ENV['API_KEY']
    client = HTTPClient.new
    url = ENV['API_URL']
    query = "#{url}?lat=#{lat}&lon=#{lon}&APPID=#{api_key}"
    response = client.get query
    data = JSON.parse response.body
    data['current']['temp'] -273
  end

end
