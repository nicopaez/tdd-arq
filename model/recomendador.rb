class Recomendador

  def initialize(adapter_clima)
    @adapter_clima = adapter_clima
  end

  def recomendar()
    @adapter_clima.get_temperatura(-34, -58).round
  end

end
